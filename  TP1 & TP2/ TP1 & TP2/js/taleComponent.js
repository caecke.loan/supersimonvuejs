Vue.component('tale-button', {
    props: ['id', 'color', 'label', 'audio', 'canBeActivated'],
    data: function () {
        return {
            isSelected: false
        }
    },
    template: `
            <div :id="id" v-on:click='playerClicOnTale()' :style="{ backgroundColor: this.isSelected ? this.color : \'\'}">{{ label }}</div>
    `,

    methods: {
        /**
         * Déclenché au clic du joueur sur une tuile.
         * Affiche la tuile.
         */
        async playerClicOnTale() {
            if (!this.canBeActivated) { return; }

            // Le joueur clique sur une case.
            this.displaySelectedTale();
        },

        /**
         * Affiche la tuile selectionnée en lui appliquant un instant une couleur de fond. 
         */
        displaySelectedTale: function () {
            this.isSelected = true;
            this.playSound(this.audio);

            setTimeout(() => {
                this.isSelected = false;
            }, 250);
        },

        /**
         * Joue un son. 
         */
        playSound() {
            if (this.audio) {
                var sound = new Audio(this.audio);
                sound.play();
            }
        }
    }
});