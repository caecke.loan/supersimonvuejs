import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import GamesList from '../views/GameList.vue'
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/games',
    name: 'Games',
    component: GamesList
  },
  {
    path:'/super-simon',
    name:'SuperSimon',
    component: () => import(/* webpackChunkName: "SimonJeuContainer" */ '../views/games/SimonJeuContainer.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
