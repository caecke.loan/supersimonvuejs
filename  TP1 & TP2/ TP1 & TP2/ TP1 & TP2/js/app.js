// Returns a Promise that resolves after "ms" Milliseconds
const timer = ms => new Promise(res => setTimeout(res, ms));

new Vue({
    el: '#app',
    data: {
        levelNumber: 1,
        isGameRunning: false,
        canPlayerPlay: false,
        gameStateLabel: 'En attente de lancement',
        simonTales: [
            { id: "simon-btn-1",  color: "brown",      label: "Clap",           isSelected: false, audio:'audio/Nox_Clap_Reverb.wav' },
            { id: "simon-btn-2",  color: "blue",       label: "Kick",           isSelected: false, audio:'audio/Nox_Kick_Club.wav' },
            { id: "simon-btn-3",  color: "blueviolet", label: "Charleston",     isSelected: false, audio:'audio/Nox_Open_Hats_Phazer.wav' },
            { id: "simon-btn-4",  color: "darkgreen",  label: "Caisse claire",  isSelected: false, audio:'audio/Nox_Snare_Migos.wav' },
        ],
        levelIndexes: [],
        playerIndexPlaying: 0
    },
    methods: {
        /**
         * Initialise une partie avec des valeurs par défaut.
         */
        async startGame() {
            this.isGameRunning = true;
            this.levelIndexes = [];
            this.playerIndexPlaying = 0;
            this.levelNumber = 1;
            this.gameStateLabel = "En cours";
            await timer(250);
            this.gameLoop();
        },
        /**
         * Boucle de jeu.
         * Affiche les derniers coups joués (niveaux précédents) et ajoute un nouveau coup pour le niveau actuel.
         */
        async gameLoop() {
            this.playerIndexPlaying = 0;
            
            this.canPlayerPlay = false;

            // Rewind des levels précédents
            for (let index = 0; index < this.levelIndexes.length; index++) {
                this.displaySelectedTale(this.simonTales[this.levelIndexes[index]]);
                await timer(380);
            }

            // Ajout d'un nouveau coup de l'ordinateur
            const colorId = this.selectRandomColorId();
            this.levelIndexes.push(colorId);
            this.displaySelectedTale(this.simonTales[colorId]);
            await timer(380);

            setTimeout(() => {
                this.canPlayerPlay = true;
            }, this.levelIndexes.length * 50);

        },
        /**
         * Déclenché au clic du joueur sur une tuile.
         * Affiche la tuile selectionnée et applique les règles du jeu.
         * @param {*} tale La tuile cliquée.
         */
        async playerClicOnTale(tale) {
            if(!this.canPlayerPlay) { return;}

            // Le joueur clique sur une case.
            this.displaySelectedTale(tale);
            // On vérifie que la case cliquée est bien la bonne.
            if (this.isClickedTaleTheGoodOne(tale.id)) {
                this.playerIndexPlaying++;

                // Si toutes les cases ont été cliquées dans le bon ordre
                if (this.playerIndexPlaying === this.levelIndexes.length) {
                    // on passe au niveau suivant
                    this.levelNumber++;
                    await timer(750);

                    this.gameLoop();
                }
            } else {
                // On annonce la défaite sinon.
                this.defeat();
            }
        },
        /**
         * Déclenché en cas d'erreur.
         * Termine la partie et averti l'utilisateur par le biais d'un message.
         */
        defeat() {
            this.gameStateLabel = "C'est perdu .......";
            this.isGameRunning = false;
        },
        /**
         * Affiche la tuile selectionnée en lui appliquant un instant une couleur de fond. 
         * @param {} tale 
         */
        displaySelectedTale: function (tale) {
            tale.isSelected = true;
            this.playSound(tale.audio);
            
            setTimeout(() => {
                tale.isSelected = false;
            }, 250);
        },
        /**
         * Vérifie que la case cliquée correspond à celle demandée par l'ordinateur en prennant en compte l'ordre de demande du jeu.
         * @param {*} taleId 
         */
        isClickedTaleTheGoodOne: function (taleId) {
            return this.levelIndexes[this.playerIndexPlaying] + 1 == taleId.split('-')[2];
        },
        /**
         * Renvoi un chiffre entre 0 et 3
         */
        selectRandomColorId: function () {
            return Math.floor(Math.random() * 4);
        },
        /**
         * Joue un son. 
         * @param {*} sound String exemple : 'assets/Nox_Clap_Reverb.wav'
         */
        playSound (sound) {
            if(sound) {
              var audio = new Audio(sound);
              audio.play();
            }
          }
    },
});