Vue.component('app-simon-header', {
    props: ['levelNumber', 'gameStateLabel', 'canPlayerPlay', 'isGameRunning'],
    template: `
        <div class="simon-header">
            <h1>Level {{ levelNumber }}</h1>
            <h3>Etat de la partie : {{ gameStateLabel }} </h3>
            <h3 v-if="isGameRunning">{{ canPlayerPlay ? "Le joueur peut jouer." : "L'ordi joue." }}</h3>
            <button @click="$emit('start-event')" :disabled="isGameRunning">Lancer la partie</button>
        </div>
    `
});