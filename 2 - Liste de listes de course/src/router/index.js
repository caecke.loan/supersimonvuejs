import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ShoppingListList from '../views/ShoppingListList.vue'
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/listing',
    name: 'Listing',
    component: ShoppingListList
  },
  {
    path:'/new',
    name:'CreateList',
    component: () => import(/* webpackChunkName: "CreateList" */ '../views/CreateListPage.vue')
  },
  {
    path: '/list/:slug',
    name: 'ShoppingListPage',
    component: () => import(/* webpackChunkName: "ShoppingListPage" */ '../views/ShoppingListPage.vue')
  },
  {
    path: '/list/:slug/add',
    name: 'ShoppingListAddPage',
    component: () => import(/* webpackChunkName: "ShoppingListAddPage" */ '../views/ShoppingListAddPage.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
